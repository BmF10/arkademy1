<?php

inputdata();

function validateusername($username)
{
    $uppercase = preg_match('/^[A-Z]+$/', $username);

    if (!$uppercase || strlen($username) < 7) {
        echo "Harus huruf besar dan minimal 7 karakter\n\n";
        return false;
    } else {
        return true;
    }
}

function validatepassword($password)
{
    $angka = substr($password, 0, -4);
    $simbol = substr($password, 3, -3);
    $huruf = substr($password, 4);

    $angkaregex = preg_match('/^[0-9]+$/', $angka);
    $simbolregex = preg_match('/([*])/', $simbol);
    $hurufregex = preg_match('/^[a-z]+$/', $huruf);
    $hurufsama = validatesama($huruf);
    $angkasama = validatesama($angka);

    if (!$angkaregex || !$hurufregex || strlen($huruf) != 3 || strlen($angka) != 3 || !$simbolregex || !$hurufsama || !$angkasama) {
        return false;
    } else {
        return true;
    }
}

function validatesama($karakter)
{
    $karakterarray = str_split($karakter, 1);
    $count = count($karakterarray);

    $result = [];

    for ($i = 0; $i < $count; $i++) {
        for ($j = 0; $j < $count; $j++) {
            if ($karakter[$i] != $karakter[$j]) {
                array_push($result, false);
            } else {
                array_push($result, true);
            }
        }
    }

    if (count($result) != count(array_filter($result))) {
        return false;
    } else {
        return true;
    }
}

function inputdata($data = null)
{
    if ($data != null) {
        $username = $data;
    } else {
        echo "Username: ";
        $username = trim(fgets(STDIN));
    }

    if (validateusername($username) == true) {
        echo "\nPassword: ";
        $password = trim(fgets(STDIN));

        if (validatepassword($password) == false) {
            echo "Format Salah\nkombinasi dari 3 digit angka berulang, simbol “*” lalu 3 huruf berulang.";
            inputdata($username);
        } else {
            echo "\n------------------------";
            echo "\nUsername: " . $username;
            echo "\nPassword: " . $password;
        }
    } else {
        inputdata();
    }
}
