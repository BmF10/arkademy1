<?php

getdata();

function execute($name, $age)
{

  if ($name == null || $age == null) {
    echo "\nData Tidak Lengkap\nMenggunakan Data Default\n\n";
    $name = 'Bima Febriansyah';
    $age = '21';
  }

  $data = array(
    'name' => $name,
    'age' => $age,
    'address' => 'JL. Prof. M. Yamin Gg. Usaha Bersama 1, Kota Baru, Pontianak Selatan, Kalimantan Barat',
    'hobbies' => array('Bermain Game', 'Travelling', 'Nonton Film'),
    'is_married' => false,
    'list_school' => array(
      ['name' => 'SD YPWKS 1 Cilegon', 'year_in' => '2003', 'year_out' => '2009', 'major' => null],
      ['name' => 'SMPN 22 Pontianak', 'year_in' => '2009', 'year_out' => '2012', 'major' => null],
      ['name' => 'SMA Mujahidin Pontianak', 'year_in' => '2012', 'year_out' => '2015', 'major' => 'IPS'],
      ['name' => 'D3 Universitas Bina Sarana Informatika', 'year_in' => '2016', 'year_out' => '2019', 'major' => 'Sistem Informasi'],
    ),
    'skills' => array(
      ['skill_name' => 'PHP', 'level' => 'Advanced'],
      ['skill_name' => 'Java', 'level' => 'Beginner'],
      ['skill_name' => 'MySQL', 'level' => 'Advanced'],
      ['skill_name' => 'C++', 'level' => 'Beginner'],
      ['skill_name' => 'JavaScript', 'level' => 'Beginner']
    ),
    'interest_in_coding' => true,
  );

  return json_encode($data);
}

function getdata()
{


  echo "Masukan Nama: ";
  $name = trim(fgets(STDIN));
  echo "Masukan Umur: ";
  $age = trim(fgets(STDIN));

  $json = execute($name, $age);
  echo $json;
}
