<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Model');
	}
	public function index()
	{
		$data = [
			'cashier' => $this->db->get('cashier')->result(),
			'category' => $this->db->get('category')->result(),
			//'product' => $this->Model->getdata()->result(),
		];

		$this->load->view('welcome_message', $data);
	}

	public function getdata()
	{
		$data = $this->Model->getdata()->result();

		echo json_encode($data);
	}

	public function add()
	{
		$data = [
			'name' => $this->input->post('product'),
			'price' => $this->input->post('price'),
			'id_category' => $this->input->post('category'),
			'id_cashier' => $this->input->post('cashier'),
		];

		$this->db->insert('product', $data);

		echo json_encode($data);
	}

	public function show($id)
	{
		$data = $this->Model->getdata1($id)->row();

		echo json_encode($data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('product');
	}
}
