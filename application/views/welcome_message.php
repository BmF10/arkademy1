<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Arkademy | Coffeshop</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js">
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>

<body>
	<style>
		.mybutton {
			background-color: #FF8FB2;
			color: white;
			padding-left: 50px;
			padding-right: 50px;
		}

		table {
			border-collapse: separate;
			/* border: solid #CECECE 1px; */
			border-radius: 20px;
			-moz-border-radius: 1px;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 19px;
		}

		td,
		th {
			padding: 25px 25px 25px;
			font-size: 20px;
		}

		th {
			background-color: #CECECE;
			border: solid #CECECE 1px;
			color: white;
			text-align: center;
		}

		th:first-child {
			border-top-left-radius: 13px;
		}

		th:last-child {
			border-top-right-radius: 13px;
		}

		td:first-child {
			font-weight: bold;
		}

		.modal-content {
			-webkit-border-radius: 0px !important;
			-moz-border-radius: 0px !important;
			border-radius: 13px !important;
		}

		.modal-header {
			border-bottom: 0px !important;
		}
	</style>

	<nav class="navbar navbar-expand-lg navbar-light shadow">
		<a class="navbar-brand" href="#">
			<img src="https://www.arkademy.com/img/logo%20arkademy.1c82cf5c.svg" width="80" height="80" alt="">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<h3 class="nav-link" style="color: #FF8FB2;">ARKADEMY <span style="color: black;">COFFEE SHOP</span>
					</h3>
				</li>

			</ul>
			<div class="form-inline" style="margin-right: 50px;">

				<button class="btn btn-lg mybutton shadow" data-toggle="modal" data-target=".bd-example-modal-lg" type="button">ADD</button>
			</div>
		</div>
	</nav>
	<div class="container p-5">
		<div class="row">
			<div class="col-md-11 mx-auto">
				<table id="table" class="shadow">
					<thead>
						<tr>
							<th>No</th>
							<th>Cashier</th>
							<th>Product</th>
							<th>Category</th>
							<th>Price</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="modal fade bd-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered">
			<div class="modal-content ">
				<div class="modal-header">
					<h3 class="modal-title">ADD</h3>
					<button style="color: #F6404F;" type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="POST" id="formAdd">
						<div class="row">
							<div class="col-md-8 mx-auto">
								<div class="form-group">
									<select class="form-control" name="cashier" id="cashier">
										<option value="" selected disabled>Choose Cashier</option>
										<?php foreach ($cashier as $c) : ?>
											<option value="<?= $c->id ?>"><?= $c->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="form-group">
									<select class="form-control" name="category" id="category">
										<option value="" selected disabled>Choose Category</option>
										<?php foreach ($category as $c) : ?>
											<option value="<?= $c->id ?>"><?= $c->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="product" name="product" placeholder="Food Name">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="price" name="price" placeholder="Price">
								</div>
							</div>
						</div>
						<input type="hidden" value="add" name="action" id="action">
						<input type="hidden" name="hidden_id" id="hidden_id">
						<button id="btnsubmit" class="btn mybutton float-right">ADD</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {

			$('#modal').on('hidden.bs.modal', function() {
				$('.modal-title').text('ADD');
				$('#btnsubmit').text('ADD');
				$('#action').val('add');
			});

			$(document).on('click', '.btndel', function() {
				user_id = $(this).attr('id');
				console.log(window.location.origin + '/arkademy1/welcome/delete/' + user_id);
				$.get(window.location.origin + '/arkademy1/welcome/delete/' + user_id);

				setTimeout(() => {
					$('#table tbody tr').remove();
					gettable();
				}, 150);

				Swal.fire({
					title: 'Berhasil Dihapus!',
					icon: 'success',
					showConfirmButton: false,
					timer: 1500
				})
			})

			$(document).on('click', '.btnedit', function() {
				var id = $(this).attr('id');
				console.log(id);
				$.ajax({
					url: window.location.origin + '/arkademy1/welcome/show/' + id,
					dataType: "json",
					success: function(result) {
						$('#product').val(result.name);
						$('#price').val(result.price);

						category = '<option value=' + result.id_category + 'selected>' + result.categoryname + '</option>'
						$('#category').append(category);

						// $('#eceran').val(html.data.harga_eceran);
						// $('#reseller').val(html.data.harga_reseller);
						// $('#kosinasi').val(html.data.harga_kosinasi);
						// $('#hidden_id').val(html.data.id);
						$('#modal').modal('show');
						$('.modal-title').text('EDIT');
						$('#btnsubmit').text('EDIT');
						$('#action').val('edit');

						console.log(result);
					}
				})
			})


			$('#formAdd').on('submit', function(event) {
				event.preventDefault();
				if ($('#action').val() == 'add') {
					$.ajax({
						url: window.location.origin + '/arkademy1/welcome/add',
						method: 'POST',
						data: new FormData(this),
						contentType: false,
						cache: false,
						processData: false,
						dataType: "json",
						success: function(data) {
							$('#table tbody tr').remove();
							gettable();
							$('#modal').modal('hide');
						}
					})
				} //1
				if ($('#action').val() == 'edit') {
					$.ajax({
						url: window.location.origin + '/arkademy1/welcome/update',
						method: 'POST',
						data: new FormData(this),
						contentType: false,
						cache: false,
						processData: false,
						dataType: "json",
						success: function(data) {
							gettable();
						}
					})
				}
			})

			function gettable() {
				$.ajax({
					url: window.location.origin + '/arkademy1/welcome/getdata',
					dataType: "json",
					success: function(result) {
						for (let index = 0; index < result.length; index++) {
							no = index + 1;

							html = '<tr>'
							html += '<td>' + no + '</td>';
							html += '<td>' + result[index].cashiername + '</td>';
							html += '<td>' + result[index].productname + '</td>';
							html += '<td>' + result[index].categoryname + '</td>';
							html += '<td>' + result[index].price + '</td>';
							html += '<td><button class="btn btn-link btnedit" id="' + result[index].productid + '" style="color: lightgreen;">Edit</button>|<button id="' + result[index].productid + '" class="btn btn-link btndel" style="color: #F6404F;">Delete</button></td>';
							html += '</tr>'
							$('#table tbody').append(html);
						}
					}
				})
			}
			gettable();
		})
	</script>
</body>

</html>