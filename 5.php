<?php

echo "Jumlah: ";
$jumlah = trim(fgets(STDIN));
createTriangle($jumlah);

function createTriangle($jumlah)
{
    for ($i = 0; $i <= $jumlah; $i++) {
        for ($j = $jumlah - 1; $j >= $i; $j--) {
            echo " ";
        }
        for ($k = 1; $k <= $i; $k++) {
            echo "*";
        }
        echo "\n";
    }
}
