
<?php
function findPair($data)
{
    if (!is_array($data)) {
        echo "Bukan Array!";
        die;
    }

    $count = count($data);

    $result = [];

    for ($i = 0; $i < $count; $i++) {
        for ($j = $i + 1; $j < $count; $j++) {
            if ($data[$i] == $data[$j]) {
                array_push($result, $data[$i]);
            }
        }
    }

    echo count(array_unique($result));
}

$input = [1, 5, 5, 10, 9, 13, 5, 1];

findPair($input);

?>
