<?php

input();

function input()
{
    echo "Kalimat: ";
    $kalimat = trim(fgets(STDIN));
    echo cek_kata($kalimat);
}

function cek_kata($kalimat)
{
    $array = explode(" ", $kalimat);

    $result = [];

    foreach ($array as $a) {
        $isword = preg_match('/^[a-z A-Z]+$/', $a);
        if ($isword == true) {
            array_push($result, $a);
        }
    }

    $return = count($result) . '/' . count($array);

    return $return;
}
